// console.log("Hello World");

//Objective 1
let string = 'supercalifragilisticexpialidocious';
console.log(string);
let filteredString = '';

//Add code here

for(let f = 0; f < string.length; f++){
	if(
		string[f] == "a" ||
		string[f] == "e" ||
		string[f] == "i" ||
		string[f] == "o" ||
		string[f] == "u" 
		){
			continue;
	}else{
		filteredString += string[f];
	}
}
console.log(filteredString);


//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        filteredString: typeof filteredString !== 'undefined' ? filteredString : null

    }
} catch(err){

}